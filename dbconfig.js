
module.exports = {

    dbConnect: function (query, callback) {

        var pg = require('pg');
        var ConnUrl = "postgres://congos:congos12345@congos.cb41hnfyyx8g.ap-south-1.rds.amazonaws.com:5432/congosapi";
        var client = new pg.Client(ConnUrl);
        client.connect(function (err) {
            if (err) {
                console.log("not connecting");
                client.end();
                callback(err);
            }
            client.query(query, function (err, result) {
                if (err) {
                    callback(err);
                }
                client.end();
                if (result == undefined) {
                    callback(err, null);
                } else {
                    callback(null, result);
                }
            });
        });
    }
}